	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de paradas de autobús <i class="fa fa-angle-right"></i> Ver paradas de autobús</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
                <h1>Lista de paradas de autobús registradas</h1>
                <hr/>
                <a href="<?php echo base_url(); ?>ingresarparada" class="btn btn-primary">Ingresar nueva para de autobús</a>
                <br/>
                <br/>
                <div class="content-panel">
                          <h4><i class="fa fa-angle-right"></i> Tabla de datos</h4><hr>
                          
                          <table class="table table-striped table-advance table-hover">  
                            <thead>
                              <tr>
                                  <th> Nombre de parada de autobús</th>
                                  <th class="hidden-phone"> Descripción</th>
                                  <th><i class="fa fa-bookmark"></i> Coordenada (Latitud)</th>
                                  <th><i class="fa fa-bookmark"></i> Coordenada (Longitud)</th>
                                  <th><i class=" fa fa-edit"></i> Acciones</th>
                              </tr>
                            </thead>
                              <?php 
                                if($table == null){
                              ?>
                                  <tbody>
                                    <tr>
                                      <td colspan="5">No hay datos que mostrar...</td>
                                    </tr>
                                  </tbody>                                

                              <?php
                                }
                                else{
                              ?>
                                  <tbody>
                                    <?php 
                                      foreach ($table as $row) {
                                        echo "<tr>";
                                        echo "<td>".$row["name"]."</td>";
                                        echo "<td>".$row["description"]."</td>";
                                        echo "<td>".$row["latitude"]."</td>";
                                        echo "<td>".$row["lenght"]."</td>";
                                        echo '<td>
                                                <a title="Ver ubicación en el mapa" class="btn btn-primary btn-xs" target="popup" href="'.base_url().'html/popup_map.htm?latitude='.$row["latitude"].'&lenght='.$row["lenght"].'" onClick="window.open(this.href, this.target, \'width=600,height=500\'); return false;"><i class="fa fa-search"></i></a>
                                                <a class="btn btn-primary btn-xs" href="'.base_url().'modificarparada/'.$row["id"].'"><i class="fa fa-pencil"></i></a>
                                                <button onClick="eliminar('.$row["id"].')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                            </td>';
                                        echo "</tr>";
                                      }


                                    ?>
                                  </tbody>
                              <?php
                                }
                              ?>

                          </table>
                      </div>



          		</div>
          	</div>
			
		      </section>
	     </section><!-- /MAIN CONTENT -->

       <style type="text/css">
         #modal_eliminar{
          display: none;
          position: absolute;
          z-index: 999;
          width: 20%;
          height: auto;
          top: 40%;
          left: 40%;
          padding: 1%;
          box-shadow: 1px 1px 10px;
          text-align: center;
          background: white;
         }
       </style>

       <div id="modal_eliminar">
         <h3>¿Esta seguro que desea eliminar el registro?</h3>
         <form method="POST" action="<?php echo base_url(); ?>eliminarparada">
           <input type="hidden" name="id_del" id="id_del" value="">
           <input type="submit" class="form-control btn btn-danger" name="eliminar" value="Aceptar" >
         </form>
         <br/>
         <button id="cancelar" onclick="cancelar()" class="form-control btn btn-default"> Cancelar</button>
       </div>


       <script type="text/javascript">
         
        function eliminar(id){
          document.getElementById("modal_eliminar").style.display = "block";
          $("#id_del").val(id);
        }

        function cancelar(){
          document.getElementById("modal_eliminar").style.display = 'none';
          $("#id_del").val() = "";
        }

       </script>
	  
