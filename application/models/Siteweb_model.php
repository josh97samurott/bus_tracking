<?php
/**
 * 
 	CLASE PARA LA ADMINISTRACIÓN DE RUTAS DE TRANSPORTE
 */
class Siteweb_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->bustracking = $this->load->database('bustracking', TRUE);
        $this->load->library("session");
	}

	public function Obtener_colores(){
		$sql = "SELECT * FROM bustracking_frontpagecolors";

		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}
		else{
			return null;
		}
	}

	public function Obtener_info_website(){
		$sql = "SELECT * FROM bustracking_frontpagedata";

		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}
		else{
			return null;
		}
	}

	public function Modificar_colores($data){
		$sql = "UPDATE bustracking_frontpagecolors SET bg_header = '".$data[0]."', text_header = '".$data[1]."', bg_body = '".$data[2]."', bg_footer = '".$data[3]."', text_footer = '".$data[4]."', color_text_decoration = '".$data[5]."' WHERE id = 0";

		if($this->bustracking->query($sql) === TRUE){
			return "Los colores del sitio público se han actualizado con exito.";
		}
		else{
			return "Ha ocurrido un inconveniente al actualizar los colores del sitio público.";
		}
	}

	public function Modificar_info_website($data){
		$sql = "UPDATE bustracking_frontpagedata SET titlepage = '".$data[0]."', phone = '".$data[1]."', email = '".$data[2]."' WHERE id = 0";

		if($this->bustracking->query($sql) === TRUE){
			return "Los datos generales del sitio público se han actualizado con exito.";
		}
		else{
			return "Ha ocurrido un inconveniente al actualizar los datos generales del sitio público.";
		}
	}
}
?>