<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontpage extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper("url");
        $this->load->library("session");
    }


    public function index(){
    	$this->load->model("Rutas_model");
        $this->load->model("Siteweb_model");
    	$data["rutas"] = $this->Rutas_model->Ver_rutas();
        $data["colors"] = $this->Siteweb_model->Obtener_colores();
        $data["website"] = $this->Siteweb_model->Obtener_info_website();
        $data["menu"] = 1;

    	$this->load->view("frontpage/template/header", $data);
    	$this->load->view("frontpage/index", $data);
    	$this->load->view("frontpage/template/footer", $data);
    }

    public function detalle_ruta($id_ruta){
    	$this->load->model("Rutas_model");
        $this->load->model("Siteweb_model");
        $data["menu"] = null;
 
        if(isset($_POST["submit"])){
            if($_POST["textMessage"] != null){
                $data["comment_status"] = $this->Rutas_model->Ingresar_comentario($id_ruta, $_POST["textMessage"]);
                $data["error"] = null;
            }
            else{
                $data["error"] = "El comentario es requerido.";
                $data["comment_status"] = null;
            }
        }
        else{
            $data["error"] = null;
            $data["comment_status"] = null;
        }

    	$data["ruta"] = $this->Rutas_model->Ver_rutas($id_ruta);
    	$data["paradas"] = $this->Rutas_model->Ver_paradas_de_rutas($id_ruta);
        $data["colors"] = $this->Siteweb_model->Obtener_colores();
        $data["website"] = $this->Siteweb_model->Obtener_info_website();
        $data["comments"] = $this->Rutas_model->Obtener_comentarios($id_ruta);




    	$this->load->view("frontpage/template/header", $data);
    	$this->load->view("frontpage/detalle_ruta", $data);
    	$this->load->view("frontpage/template/footer", $data);
    }

    public function rutas(){

        $this->load->model("Rutas_model");
        $this->load->model("Siteweb_model");
        $data["rutas"] = $this->Rutas_model->Ver_rutas();
        $data["colors"] = $this->Siteweb_model->Obtener_colores();
        $data["website"] = $this->Siteweb_model->Obtener_info_website();
        $data["menu"] = 2;

        $this->load->view("frontpage/template/header", $data);
        $this->load->view("frontpage/rutas", $data);
        $this->load->view("frontpage/template/footer", $data);
    }

    public function buscar(){
        $this->load->model("Rutas_model");
        $this->load->model("Siteweb_model");

        if($_POST["keyword"] == null || $_POST["keyword"] == ''){
            $keyword = "";
        }
        else{
            $keyword = $_POST["keyword"];
        }
        $data["rutas"] = $this->Rutas_model->Buscar_rutas($keyword);
        
        $data["colors"] = $this->Siteweb_model->Obtener_colores();
        $data["website"] = $this->Siteweb_model->Obtener_info_website();
        $data["menu"] = null;

        $this->load->view("frontpage/template/header", $data);
        $this->load->view("frontpage/busqueda", $data);
        $this->load->view("frontpage/template/footer", $data);
    }

    public function contactform(){
        $this->load->model("Siteweb_model");
        $data["colors"] = $this->Siteweb_model->Obtener_colores();
        $data["website"] = $this->Siteweb_model->Obtener_info_website();
        $website = $this->Siteweb_model->Obtener_info_website();
        $data["menu"] = 3;

        if(isset($_POST["submit"])){
            if($_POST["name"] != null){
                if($_POST["email"] != null){
                    if($_POST["subject"]){
                        if($_POST["message"]){
                            $this->load->model("Email_model");
                            $values = array("nombre" => $_POST["name"], "correo" => $_POST["email"], "asunto" => $_POST["subject"], "mensaje" => $_POST["message"], "sitio" => $website[0]["titlepage"], "correositio" => $website[0]["email"]);
                            $message = $this->Email_model->sendmail($values);    
                            $data["message"] = $message;
                        }
                    }
                }
            }
            
        }else{
            $data["message"] = null;
        }

        $this->load->view("frontpage/template/header", $data);
        $this->load->view("frontpage/contactanos", $data);
        $this->load->view("frontpage/template/footer", $data);
    }

}