	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Información de usuario administrador </h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <h1>Editar datos usuario administrativo</h1>
                <hr/>
                <form action="<?php echo base_url(); ?>infouser/<?php echo $id_user; ?>" method="POST">

                  <div class="col-lg-12">
                    <div class="showback">
                      <h4>Datos de usuario administrativo</h4><hr/>

                      <label>Nombre de usuario:</label>
                      <input type="text" name="real_username" class="form-control" value="<?php echo $real_username; ?>" />
                      <?php if($error[0] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[0]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Nombres:</label>
                      <input type="text" class="form-control" name="firstname" value="<?php echo $firstname; ?>" />
                      <?php if($error[1] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[1]."</b> </div>"; 
                      } ?>

                      <br/>                      

                      <label>Apellidos:</label>
                      <input type="text" class="form-control" name="lastname" value="<?php echo $lastname; ?>" />
                      <?php if($error[2] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[2]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Correo eléctronico:</label>
                      <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" />
                      <?php if($error[3] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[3]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Contraseña: (Si no desea actualizar su contraseña dejar vacio este campo)</label>
                      <input type="password" name="password" class="form-control" name="password" />

                      <br/>
                      
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="Editar datos de usuario" />
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <a href="<?php echo base_url(); ?>backend" class="btn btn-default btn-lg btn-block" />Cancelar </a>
                    </div>
                  </div>
                  
                  <br/>
                
                </form>


          		</div>
          	</div>
			 
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->


    