<!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Contáctanos</h1>
            <!--span class="color-text-a">Grid Properties</span-->
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>inicio">Inicio</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Contáctanos
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->


<!--/ Contact Star /-->
  <section class="contact">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 section-t8">
          <div class="row">
            <div class="col-md-7">
              <form class="form-a" action="<?php echo base_url(); ?>contactanos" method="POST" >
                <?php if($message != null){ ?>
                    <label style="color: white;" class="btn btn-success"><?php echo $message; ?></label>
                    <br/>
                <?php } ?>
                    
                <div class="row">
                  <div class="col-md-6 mb-3">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control form-control-lg form-control-a" placeholder="Tu nombre" data-rule="minlen:4" data-msg="Por favor ingresa mínimo 4 caracteres" required>
                    </div>
                  </div>
                  <div class="col-md-6 mb-3">
                    <div class="form-group">
                      <input name="email" type="email" class="form-control form-control-lg form-control-a" placeholder="Tu correo eléctronico" data-rule="email" data-msg="Por favor ingresa un correo eléctronico valido" required>
                      
                    </div>
                  </div>
                  <div class="col-md-12 mb-3">
                    <div class="form-group">
                      <input type="text" name="subject" class="form-control form-control-lg form-control-a" placeholder="Asunto" data-rule="minlen:4" data-msg="Por favor ingresa al menos 8 caracteres para el asunto" required />
                    </div>
                  </div>
                  <div class="col-md-12 mb-3">
                    <div class="form-group">
                      <textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="Por favor escribe un mensaje" placeholder="Mensaje" required></textarea>
                      
                    </div>
                  </div>
                  <div class="col-md-12">
                    <input type="submit" class="btn btn-a" name="submit" id="submit" value="Enviar Mensaje" />
                    
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-5 section-md-t3">
              <div class="icon-box section-b2">
                <div class="icon-box-icon">
                  <span class="ion-ios-paper-plane"></span>
                </div>
                <div class="icon-box-content table-cell">
                  <div class="icon-box-title">
                    <h4 class="icon-title">Saludanos</h4>
                  </div>
                  <div class="icon-box-content">
                    <p class="mb-1">Correo.
                      <span class="color-a"><?php echo $website[0]["email"]; ?></span>
                    </p>
                    <p class="mb-1">Télefono.
                      <span class="color-a"><?php echo $website[0]["phone"]; ?></span>
                    </p>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Contact End /-->