<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Servicio de transporte | <?php echo $website[0]["titlepage"]; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url(); ?>assets/img/favicon.ico' />

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo base_url(); ?>assets/frontpage/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php echo base_url(); ?>assets/frontpage/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontpage/lib/animate/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontpage/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/frontpage/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo base_url(); ?>assets/frontpage/css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: EstateAgency
    Theme URL: https://bootstrapmade.com/real-estate-agency-bootstrap-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <div class="click-closed"></div>
  <!--/ Form Search Star /-->
  <div class="box-collapse">
    <div class="title-box-d">
      <h3 class="title-d">Buscar una ruta de autobús</h3>
    </div>
    <span class="close-box-collapse right-boxed ion-ios-close"></span>
    <div class="box-collapse-wrap form">
      <form class="form-a" action="<?php echo base_url(); ?>buscador" method="POST">
        <div class="row">
          <div class="col-md-12 mb-2">
            <div class="form-group">
              <label for="Type">Ingrese una palabra clave</label>
              <input type="text" name="keyword" class="form-control form-control-lg form-control-a" placeholder="Plabra clave">
            </div>
          </div>
          
          
          <div class="col-md-12">
            <button type="submit" name="buscar" class="btn btn-b">Buscar ruta</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!--/ Form Search End /-->

  <!--/ Nav Star /-->
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="<?php echo base_url(); ?>inicio"><?php echo $website[0]["titlepage"]; ?></a>
      <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link <?php if($menu == 1){ echo "active"; } ?>" href="<?php echo base_url(); ?>inicio">Inicio</a>
          </li>
          <!--li class="nav-item">
            <a class="nav-link" href="<?php echo base_url(); ?>nosotros">Sobre nosotros</a>
          </li-->
          <li class="nav-item">
            <a class="nav-link <?php if($menu == 2){ echo "active"; } ?>" href="<?php echo base_url(); ?>rutas">Rutas de transporte</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if($menu == 3){ echo "active"; } ?>"  href="<?php echo base_url(); ?>contactanos">Contáctanos</a>
          </li>
        </ul>
      </div>
      <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
        data-target="#navbarTogglerDemo01" aria-expanded="false">
        <span class="fa fa-search" aria-hidden="true"></span>
      </button>
    </div>
  </nav>
  <!--/ Nav End /-->

<style type="text/css">
  .color-b{
    color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .text-brand:hover{
    color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .navbar-default.navbar-reduce .nav-link:before{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .navbar-default.navbar-trans .nav-link:before, .navbar-default.navbar-reduce .nav-link:before{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .btn.btn-b-n{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .card-footer-a{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .card-info li span{
    color: black;
  }

  .price-a{
    border-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .back-to-top{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .section-footer .item-list-a i{
    color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .section-footer .item-list-a a:hover{
    color: #000000 !important;
  }

  .list-inline-item a:hover{
    color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .btn.btn-b{
    background-color: black !important;
    color: white;
  }

  .btn.btn-b:hover, .btn-a:hover{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
    color: white !important;
  }

  .title-box-d .title-d:after{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .nav-pills-a.nav-pills .nav-link.active:after{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .card-box-ico{
    border-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .intro-single .title-single-box{
    border-left-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }

  .navbar-default, .navbar-default a{
    background-color: <?php echo $colors[0]["bg_header"]; ?> !important;
    color: <?php echo $colors[0]["text_header"]; ?> !important;
  }

  body{
    background-color: <?php echo $colors[0]["bg_body"]; ?> !important;
  }

  .section-footer, footer{
    background-color: <?php echo $colors[0]["bg_footer"]; ?> !important;
  }

  .section-footer h3, .section-footer a, .section-footer p, .section-footer li, .section-footer span, footer a, footer p, footer span, footer .credits{
    color: <?php echo $colors[0]["text_footer"]; ?> !important;
  }

  .owl-theme .owl-dots .owl-dot.active span{
    background-color: <?php echo $colors[0]["color_text_decoration"]; ?> !important;
  }
</style>