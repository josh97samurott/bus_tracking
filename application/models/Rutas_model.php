<?php

/**
 * 
 	CLASE PARA LA ADMINISTRACIÓN DE RUTAS DE TRANSPORTE
 */
class Rutas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->bustracking = $this->load->database('bustracking', TRUE);
        $this->load->library("session");
	}

	public function Ver_rutas($id = null){
		if($id == null){
			$sql = "SELECT * FROM bustracking_busroutes";
			$query = $this->bustracking->query($sql);

			if($query->num_rows() > 0){
				$data = $query->result_array();
				return $data;
			}
			else{
				return null;
			}
		}
		else{
			$sql = "SELECT * FROM bustracking_busroutes WHERE id = ".$id;
			$query = $this->bustracking->query($sql);

			if($query->num_rows() > 0){
				$data = $query->result_array();
				return $data;
			}
			else{
				return null;
			}
		}
	}

	public function Buscar_rutas($keyword){
		$sql = "SELECT DISTINCT a.* 
				FROM `bustracking_busroutes` as a inner join bustracking_routes_rel_stops as b on a.id = b.id_routes 
				inner join bustracking_busstops as c on b.id_stops = c.id 
				WHERE a.name like '%".$keyword."%' OR a.description like '%".$keyword."%' OR a.driver like '%".$keyword."%' 
				OR a.license_plate like '%".$keyword."%' OR a.price like '%".$keyword."%' OR a.departure_time like '%".$keyword."%' 
				OR a.return_time like '%".$keyword."%' OR b.arrival_time like '%".$keyword."%' OR b.departure_time like '%".$keyword."%' 
				OR c.name like '%".$keyword."%' OR c.description like '%".$keyword."%'";

		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}
		else{
			return null;
		}
		
	}

	public function Ver_paradas_de_rutas($id_route){
		$sql = "SELECT c.id, b.departure_time, b.arrival_time, c.name, c.description, c.latitude, c.lenght FROM bustracking_busroutes AS a INNER JOIN bustracking_routes_rel_stops AS b ON a.id = b.id_routes INNER JOIN bustracking_busstops AS c ON b.id_stops = c.id WHERE a.id = ".$id_route;
		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}
		else{
			return null;
		}
	}

	public function preload_data(){
		$sql = "SELECT * FROM 	bustracking_busstops";
		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}else{
			return null;
		}
	}

	public function Ingresar_ruta($data, $info_stops){
		$sql = "INSERT INTO bustracking_busroutes VALUES(null, '".$data[0]."', '".$data[1]."', '".$data[2]."', '".$data[3]."', '".$data[4]."', '".$data[5]."', '".$data[6]."')";

		if($this->bustracking->query($sql) === TRUE){
			$sql = "SELECT * FROM bustracking_busroutes ORDER BY id DESC limit 1";

			$query = $this->bustracking->query($sql);
			$id = null;
			
			foreach ($query->result_array() as $row ) {
				$id = $row["id"];
			};

			if($id != null){
				$n = count($info_stops);

				for($i = 0; $i < $n; $i++){
					$string = explode("|", $info_stops[$i]);
					$id_stops = $string[0];
					$hour_ll = $string[1];
					$hour_s = $string[2];
					//$day = $string[3];

					$sql = "INSERT INTO bustracking_routes_rel_stops VALUES(null, ".$id.", ".$id_stops.", '".$hour_ll."', '".$hour_s."')";
					$this->bustracking->query($sql);
				}

				return "La ruta de autobús ha sido ingresado correctamente.";

			}
			else{
				return "Ha ocurrido un inconveniente al intentar ingresar las paradas de la ruta de autobús, por favor intentarlo de nuevo.";
			}

		}
		else{
			return "Ha ocurrido un inconveniente al intentar ingresar la ruta de autobús, por favor intentarlo de nuevo.";
		}
	}

	public function Modificar_ruta($data, $info_stops, $id_route){
		$sql = "UPDATE bustracking_busroutes SET name = '".$data[0]."', description = '".$data[1]."', driver = '".$data[2]."', license_plate = '".$data[3]."', price = '".$data[4]."', departure_time = '".$data[5]."', return_time = '".$data[6]."' WHERE id = ".$id_route;

		if($this->bustracking->query($sql) === TRUE){
			$sql = "DELETE FROM bustracking_routes_rel_stops WHERE id_routes = ".$id_route;
			$this->bustracking->query($sql);

			$n = count($info_stops);

			for($i = 0; $i < $n; $i++){
				$string = explode("|", $info_stops[$i]);
				$id_stops = $string[0];
				$hour_ll = $string[1];
				$hour_s = $string[2];

				$sql = "INSERT INTO bustracking_routes_rel_stops VALUES(null, ".$id_route.", ".$id_stops.", '".$hour_ll."', '".$hour_s."')";
				$this->bustracking->query($sql);
			}

			return "La ruta de autobús ha sido actualizada correctamente.";
		}
		else{
			return "Ha ocurrido un inconveniente al intentar actualizar la ruta de autobús, por favor intentarlo de nuevo.";
		}

	}

	public function Eliminar_ruta($id){
		$sql = "DELETE FROM bustracking_routes_rel_stops WHERE id_routes = ".$id;

		if($this->bustracking->query($sql) === TRUE){
			$sql = "DELETE FROM bustracking_busroutes WHERE id = ". $id;	

			if($this->bustracking->query($sql) === TRUE){
				return "El registro se ha eliminado con exito.";
			}
			else{
				return "Ha ocurrido un inconveniente al intentar eliminar la ruta de autobús seleccionada, por favor intentarlo de nuevo.";
			}
		}
		else{
			return "Ha ocurrido un inconveniente al intentar eliminar las paradas de autobús de la ruta seleccionada, por favor intentarlo de nuevo.";
		}
	}

	public function Obtener_comentarios($id){
		$sql = "SELECT * FROM bustracking_comments WHERE id_routes = ".$id;

		$query = $this->bustracking->query($sql);

		if($query->num_rows() > 0){
			$data = $query->result_array();
			return $data;
		}else{
			return null;
		}
	}

	public function Ingresar_comentario($id, $comment){
		$sql = "INSERT INTO bustracking_comments VALUES(null, ".$id.", '".$comment."', NOW() )";

		if($this->bustracking->query($sql) === TRUE){
			return "<label class='badge badge-success'>Su comentario ha sido públicado</label>";
		}
		else{
			return "<label class='badge badge-danger'>Ha ocurrido algún inconveniente al públicar su comentario.</label>";
		}
	}

}