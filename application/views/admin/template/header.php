<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url(); ?>assets/img/favicon.ico' />


    <title><?php echo $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/js/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>BUS TRACKING</b></a>
            <!--logo end-->
            
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">

                    <li><a class="logout" href="<?php echo base_url(); ?>">Ir al sitio público</a></li>
                    <li><a class="logout" href="<?php echo base_url(); ?>loggout">Cerrar sesión</a></li>
                    
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="<?php echo base_url(); ?>assets/img/logo_v1.jpg" class="img-circle" width="60"></a></p>
              	  <a href="<?php echo base_url(); ?>infouser/<?php echo $id_user; ?>" ><h5 class="centered"><?php echo $username; ?></h5></a>
              	  	
                  <li class="mt">
                      <?php if(isset($menu) && $menu == 1){?>
                      <a class="active" href="<?php echo base_url(); ?>backend">
                      <?php }
                            else{
                      ?>
                      <a href="<?php echo base_url(); ?>backend">
                      <?php } ?>
                          <i class="fa fa-group"></i>
                          <span>Consultar rutas</span>
                      </a>

                  </li>

                  <li class="sub-menu">
                      <?php if(isset($menu) && $menu == 2){?>
                      <a class="active" href="javascript:;" >
                      <?php }else{ ?>
                      <a href="javascript:;" >
                      <?php } ?>
                          <i class="fa fa-car"></i>
                          <span>Gestionar paradas </span><br/><span>de autobús</span>
                      </a>
                      <ul class="sub">
                          <li>
                            <a  href="<?php echo base_url(); ?>verparadas">Ver paradas de autobús</a></li>
                          <li><a  href="<?php echo base_url(); ?>ingresarparada">Agregar paradas de autobús</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <?php if(isset($menu) && $menu == 3){?>
                      <a class="active" href="javascript:;" >
                      <?php }else{ ?>
                      <a href="javascript:;" >
                      <?php } ?>
                          <i class="fa fa-car"></i>
                          <span>Gestionar rutas </span><br/><span>de autobús</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?php echo base_url(); ?>verrutas">Ver rutas de autobús</a></li>
                          <li><a  href="<?php echo base_url(); ?>ingresarruta">Agregar rutas de autobús</a></li>
                      </ul>
                  </li>

                  <li class="mt">
                      <?php if(isset($menu) && $menu == 4){?>
                      <a class="active" href="<?php echo base_url(); ?>datosgenerales" >
                      <?php }else{ ?>
                      <a href="<?php echo base_url(); ?>datosgenerales" >
                      <?php } ?>
                          <i class="fa fa-sitemap"></i>
                          <span>Personalizar sitio web</span>
                      </a>
                  </li>
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
