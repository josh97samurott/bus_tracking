<?php 

/**
 * 
	CLASE PARA INICIAR SESIÓN AL AREA ADMINISTRATIVA
 */

class Login_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->bustracking = $this->load->database('bustracking', TRUE);
        $this->load->library("session");
	}


	public function modificar_infouser($datos){
		if($datos[5] != null){
			$sql = "UPDATE bustracking_user SET username = '".$datos[1]."', firstname = '".$datos[2]."', lastname = '".$datos[3]."', email = '".$datos[4]."', password = '".sha1($datos[5])."' WHERE id_user = ".$datos[0];
		}
		else{
			$sql = "UPDATE bustracking_user SET username = '".$datos[1]."', firstname = '".$datos[2]."', lastname = '".$datos[3]."', email = '".$datos[4]."' WHERE id_user = ".$datos[0];
		}

		if($this->bustracking->query($sql) === TRUE){

			$set_session = array(
				'username' => $datos[1],
				'firstname' => $datos[2],
				'lastname' => $datos[3],
				'email' => $datos[4]
			);

					$this->session->set_userdata($set_session);

			return "Los datos del usuario administrador han sido actualizados";
		}
		else{
			return "Hubo un inconveniente al actualizar los datos del usuario administrador";
		}

	}

	public function log_in($username, $password){

		$query = $this->bustracking->query("SELECT * FROM bustracking_user WHERE username = '".$username."'");

		if( $query->num_rows() > 0 ){
			$data = $query->row_array();
			
			if($username === $data["username"]){
				
				if( sha1($password) === $data["password"] ){

					$set_session = array(
						'id_user' => $data["id_user"],
						'username' => $username,
						'firstname' => $data["firstname"],
						'lastname' => $data["lastname"],
						'email' => $data["email"],
						'logged_in' => TRUE
					);

					$this->session->set_userdata($set_session);

					return 0;
				}
				else{
					return 1;
				}
			}
			else{
				return 1;
			}
		}
		else{
			return 1;
		}

		return 1;
	}

	public function loggout(){
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('firstname');
		$this->session->unset_userdata('lastname');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('logged_in');
	}


	public function close_db(){
		$this->bustracking->close();
	}
}

?>