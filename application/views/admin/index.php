	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Consultar rutas <i class="fa fa-angle-right"></i> Rutas de transporte actuales</h3>
          	<br/><br/>

            <div class="row">

            <?php 
            if($table != null){
              foreach ($table as $row) { ?>
              
              
              <a href="<?php echo base_url(); ?>verdetallederutas/<?php echo $row['id']; ?> " >
              <div class="col-lg-4 col-md-4 col-sm-4 mb">
                <!-- WHITE PANEL - TOP USER -->
                <div class="white-panel pn">
                  <div class="white-header">
                    <h5 style="color: black;"><b><?php echo $row["name"]; ?></b></h5>
                  </div>
                  <p style="color: gray;"><b>Precio | $<?php echo $row["price"]; ?></b></p>
                    <div class="row">
                      <div class="col-md-6">
                        <p class="small mt" style="color: gray;">Hora de salida</p>
                        <p style="color: gray;"><?php echo $row["departure_time"]; ?></p>
                      </div>
                      <div class="col-md-6">
                        <p class="small mt" style="color: gray;">Hora de retorno</p>
                        <p style="color: gray;"><?php echo $row["return_time"]; ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <p class="small mt" style="color: gray;">Conductor</p>
                        <p style="color: gray;"><?php echo $row["driver"]; ?></p>
                      </div>
                      <div class="col-md-6">
                        <p class="small mt" style="color: gray;">Matricula</p>
                        <p style="color: gray;"><?php echo $row["license_plate"]; ?></p>
                      </div>
                    </div>
                </div>
              </div><!-- /col-md-4 -->
              </a>
              
            

            <?php 
              } 
            }?>

            </div>  

          
			
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->
	  

      