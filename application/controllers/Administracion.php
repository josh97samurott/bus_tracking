<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administracion extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper("url");
        $this->load->library("session");
        $data["id_user"] = $this->session->userdata("id_user");
    }

    /**********************************************************
                        LOGIN Y USUARIO
    **********************************************************/

	public function index()
	{
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if($this->session->userdata("logged_in") == TRUE){
                $data["title"] = "Consulta de Rutas | Administración Bus Tracking";
                $data["menu"] = 1;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                $data["id_user"] = $this->session->userdata("id_user");
                
                $this->load->model("Rutas_model");
                $data["table"] = $this->Rutas_model->Ver_rutas();
                $this->load->view("admin/template/header", $data);
                $this->load->view("admin/index");
                $data["message"] = null;
                $this->load->view("admin/template/footer", $data);    
            }
            else{
                $this->login();
            }
        }
        else{
            $this->login();
        }
    }

    public function modificarusuario($id){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if($this->session->userdata("logged_in") == TRUE){
                $data["title"] = "Información de usuario | Administración Bus Tracking";
                $data["menu"] = null;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");


                $data["real_username"] = $this->session->userdata("username");
                $data["firstname"] = $this->session->userdata("firstname");
                $data["lastname"] = $this->session->userdata("lastname");
                $data["email"] = $this->session->userdata("email");
                $data["id_user"] = $this->session->userdata("id_user");
                $data["message"] = null;

                if(isset($_POST["submit"])){

                    if($_POST["real_username"] != null){
                        
                        if($_POST["firstname"] != null){
                            
                            if($_POST["lastname"] != null){
                                
                                if($_POST["email"] != null){

                                    if($_POST["password"] != null){
                                        $values = array(0 => $this->session->userdata("id_user"), 1 => $_POST["real_username"], 2 => $_POST["firstname"], 3 => $_POST["lastname"], 4 => $_POST["email"], 5 => $_POST["password"]);
                                    }
                                    else{
                                        $values = array(0 => $this->session->userdata("id_user"), 1 => $_POST["real_username"], 2 => $_POST["firstname"], 3 => $_POST["lastname"], 4 => $_POST["email"], 5 => null);
                                    }

                                    $this->load->model("Login_model");
                                    $data["message"] = $this->Login_model->modificar_infouser($values);
                                    $error = array(0 => null, 1 => null, 2 => null, 3 => null);

                                    $data["real_username"] = $this->session->userdata("username");
                                    $data["firstname"] = $this->session->userdata("firstname");
                                    $data["lastname"] = $this->session->userdata("lastname");
                                    $data["email"] = $this->session->userdata("email");
                                    $data["id_user"] = $this->session->userdata("id_user");
                                    $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                                    
                                }
                                else{
                                    $error = array(0 => null, 1 => null, 2 => null, 3 => "El correo eléctronico del usuario no puede estar vacio");
                                }

                            }
                            else{
                                $error = array(0 => null, 1 => null, 2 => "Los apellidos del usuario no pueden estar vacios", 3 => null);
                            }

                        }
                        else{
                            $error = array(0 => null, 1 => "Los nombres del usuario no pueden estar vacios", 2 => null, 3 => null);
                        }

                    } 
                    else{
                        $error = array(0 => "El username no puede estar vacio", 1 => null, 2 => null, 3 => null);
                    }

                }
                else{
                    $error = array(0 => null, 1 => null, 2 => null, 3 => null);    
                }

                $data["error"] = $error;

                $this->load->view("admin/template/header", $data);
                $this->load->view("admin/infouser", $data);
                $this->load->view("admin/template/footer", $data);

            }
            else{
                $this->login();
            }
        }
        else{
            $this->login();
        }
    }


    /********************************************************
                GESTIÓN DE PARADAS DE AUTOBÚS
    *********************************************************/
    public function ver_paradas($status = null){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            
            if($this->session->userdata("logged_in") == TRUE ){

                if($status == 1){
                    $message = 'El registro ha sido actualizado con exito';
                }
                else{
                    $message = null;
                }

                $data["title"] = "Gestión de paradas de transporte | Administración Bus Tracking";
                $this->load->model("Paradas_model");
                $data["table"] = $this->Paradas_model->Ver_paradas();
                $data["message"] = $message;
                $data["menu"] = 2;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                $data["id_user"] = $this->session->userdata("id_user");
                $this->load->view("admin/template/header", $data);
                $this->load->view("admin/paradas/ver_paradas", $data);
                $this->load->view("admin/template/footer", $data);
            }
            else{
                redirect("login");
            }

        }
        else{
            redirect("login");
        }
    }

    public function ingresar_parada(){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){

                $data["title"] = "Gestión de paradas de transporte | Administración Bus Tracking";
                $data["menu"] = 2;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                $data["id_user"] = $this->session->userdata("id_user");
                $this->load->view("admin/template/header", $data);
                

                if(isset($_POST["submit"])){

                    if($_POST["name"] != null){
                        
                        if($_POST["description"] != null){
                            
                            if($_POST["latitude"] != null){

                                if(is_numeric($_POST["latitude"]) == TRUE){

                                    if($_POST["lenght"] != null){

                                        if(is_numeric($_POST["lenght"]) == TRUE){
                                            $error = array( 0 =>null, 1=>null, 2=>null, 3=>null);
                                            $values = array( 0 =>$_POST["name"], 1=>$_POST["description"], 2=>$_POST["latitude"], 3=>$_POST["lenght"]);
                                            $this->load->model("Paradas_model");
                                            $message = $this->Paradas_model->Ingresar_parada($values);
                                        }
                                        else{
                                            $error = array( 0 =>null, 1=>null, 2=>null, 3=>"La longitud de la ubicación geográfica ingresada no es valida");        
                                            $message = null;
                                        }

                                    }
                                    else{
                                        $error = array( 0 =>null, 1=>null, 2=>null, 3=>"La longitud de la ubicación geográfica es requerida");
                                        $message = null;
                                    }   

                                }
                                else{
                                    $error = array( 0 =>null, 1=>null, 2=>"La latitud de la ubicación geográfica ingresada no es valida", 3=>null);    
                                    $message = null;
                                }

                            }
                            else{
                                $error = array( 0 =>null, 1=>null, 2=>"La latitud de la ubicación geográfica es requerida", 3=>null);
                                $message = null;
                            }

                        }
                        else{
                            $error = array( 0 =>null, 1=>"La descripción es requerida", 2=>null, 3=>null);
                            $message = null;
                        }

                    }
                    else{
                        $error = array( 0 =>"El nombre es requerido", 1=>null, 2=>null, 3=>null);
                        $message = null;
                    }

                    $values = array( 0 =>$_POST["name"], 1=>$_POST["description"], 2=>$_POST["latitude"], 3=>$_POST["lenght"]);
                    
                }
                else{
                    $values = array( 0 =>null, 1=>null, 2=>null, 3=>null);
                    $error = array( 0 =>null, 1=>null, 2=>null, 3=>null);
                    $message = null;
                }

                $data["values"] = $values;
                $data["error"] = $error;
                $data["message"] = $message;
                $this->load->view("admin/paradas/ingresar_paradas", $data);
                $this->load->view("admin/template/footer", $data);
            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }

    public function modificar_parada($id = null){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){

                if($id != null){
                    

                    $this->load->model("Paradas_model");
                    $info = $this->Paradas_model->Obtener_datos_parada($id);
                    
                    if($info != null){

                        $data["title"] = "Gestión de paradas de transporte | Administración Bus Tracking";
                        $data["menu"] = 2;
                        $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                        $data["id_user"] = $this->session->userdata("id_user");
                        $this->load->view("admin/template/header", $data);

                        if(isset($_POST["submit"])){

                            if($_POST["name"] != null){
                        
                                if($_POST["description"] != null){
                                    
                                    if($_POST["latitude"] != null){

                                        if(is_numeric($_POST["latitude"]) == TRUE){

                                            if($_POST["lenght"] != null){

                                                if(is_numeric($_POST["lenght"]) == TRUE){
                                                    $error = array( 0 =>null, 1=>null, 2=>null, 3=>null);
                                                    $values = array( 0 =>$_POST["name"], 1=>$_POST["description"], 2=>$_POST["latitude"], 3=>$_POST["lenght"]);
                                                    $this->load->model("Paradas_model");
                                                    $message = $this->Paradas_model->Modificar_datos_parada($id, $values);
                                                }
                                                else{
                                                    $error = array( 0 =>null, 1=>null, 2=>null, 3=>"La longitud de la ubicación geográfica ingresada no es valida");        
                                                    $message = null;
                                                }

                                            }
                                            else{
                                                $error = array( 0 =>null, 1=>null, 2=>null, 3=>"La longitud de la ubicación geográfica es requerida");
                                                $message = null;
                                            }   

                                        }
                                        else{
                                            $error = array( 0 =>null, 1=>null, 2=>"La latitud de la ubicación geográfica ingresada no es valida", 3=>null);    
                                            $message = null;
                                        }

                                    }
                                    else{
                                        $error = array( 0 =>null, 1=>null, 2=>"La latitud de la ubicación geográfica es requerida", 3=>null);
                                        $message = null;
                                    }

                                }
                                else{
                                    $error = array( 0 =>null, 1=>"La descripción es requerida", 2=>null, 3=>null);
                                    $message = null;
                                }

                            }
                            else{
                                $error = array( 0 =>"El nombre es requerido", 1=>null, 2=>null, 3=>null);
                                $message = null;
                            }

                            if($message != -1 && $message != null){
                                redirect("verparadas/1");
                            }
                            else{
                                $data["id"] = $id;
                                $values = array( 0 =>$_POST["name"], 1=>$_POST["description"], 2=>$info[0]["latitude"], 3=>$info[0]["lenght"]);
                                $data["values"] = $values;
                                $data["error"] = $error;
                                $data["message"] = "Ha ocurrido un inconveniente al intentar modificar la parada de autobús seleccionada, por favor intentarlo de nuevo.";

                                $this->load->view("admin/paradas/editar_parada", $data);
                                $this->load->view("admin/template/footer", $data);
                            }

                            

                        }
                        else{
                            $values = array( 0 => $info[0]["name"], 1 => $info[0]["description"], 2 => $info[0]["latitude"], 3 => $info[0]["lenght"]);
                            $error = array( 0 => null, 1 => null, 2 => null, 3 => null);
                            $message = null;
                            $data["id"] = $id;
                            $data["values"] = $values;
                            $data["error"] = $error;
                            $data["message"] = $message;
                            $this->load->view("admin/paradas/editar_parada", $data);
                            $this->load->view("admin/template/footer", $data);
                        }
                        
                    }
                    else{

                    }

                } 
                else{
                    redirect("verparadas");
                }

            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }

    public function eliminar_parada(){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){
                $id_del = $_POST["id_del"];

                $this->load->model("Paradas_model");
                $message = $this->Paradas_model->Eliminar_parada($id_del);
                redirect("verparadas");
            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }

    /********************************************************/


    /********************************************************
                GESTIÓN DE RUTAS DE AUTOBÚS
    *********************************************************/
    
    public function ingresar_ruta(){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){

                $this->load->model("Rutas_model");
                $menu = 3;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                $data["id_user"] = $this->session->userdata("id_user");
                $data["title"] = "Gestión de rutas de transporte | Administración Bus Tracking";

                if(isset($_POST["submit"])){

                    if(isset($_POST["info_stops"]) ){
                        $info_stops = $_POST["info_stops"];
                    }
                    else{
                        $info_stops = null;
                    }

                    if($_POST["name"] != null){

                        if($_POST["description"] != null){

                            if($info_stops != null){
                                
                                if($_POST["driver"] != null){

                                    if($_POST["license_plate"] != null){

                                        if($_POST["price"] != null){

                                            if( $_POST["hour_s"] < $_POST["hour_r"] ){

                                                $values = array( 0 => $_POST["name"], 1 => $_POST["description"], 2 => $_POST["driver"], 3 => $_POST["license_plate"], 4 => $_POST["price"], 5 => $_POST["hour_s"], 6 => $_POST["hour_r"]);
                                                $error = array( 0 =>null, 1=>null, 2=>null, 3=>null, 4=>null, 5=>null, 6=>null);
                                                $message = $this->Rutas_model->Ingresar_ruta($values, $info_stops);

                                            }
                                            else{
                                                $error = array(0 => null, 1 => null, 2 => null, 3 => NULL, 4 => null, 5=>'La hora de salida del autobús debe ser menor a la hora de retorno.', 6=>'La hora de retorno del autbús debe ser mayor a la hora de salida');
                                                $message = null;   
                                            }
   
                                        }
                                        else{
                                            $error = array(0 => null, 1 => null, 2 => null, 3 => NULL, 4 => 'El precio para abordar el autobús es requerido, si está no tiene precio ingresar el valor de $00.00.', 5=>null, 6=>null);
                                            $message = null;    
                                        }

                                    }
                                    else{
                                        $error = array(0 => null, 1 => null, 2 => null, 3 => 'La matricula del autobús es requerida', 4=>null, 5=>null, 6=>null);
                                        $message = null;
                                    }

                                }
                                else{
                                    $error = array(0 => null, 1 => null, 2 => 'El nombre del conductor es requerido', 3 => null, 4=>null, 5=>null, 6=>null);
                                    $message = null;
                                }

                            }
                            else{
                                $error = array(0 => null, 1 => null, 2 => null, 3 => null, 4=>null, 5=>null, 6=>null);
                                $message = null;
                            }

                        }
                        else{
                            $error = array(0 => null, 1 => 'La descripción es requerida', 2 => null, 3 => null, 4=>null, 5=>null, 6=>null);
                            $message = null;
                        }

                    }
                    else{
                        $error = array( 0 => 'El nombre es requerido', 1 => null, 2 => null, 3 => null, 4=>null, 5=>null, 6=>null);
                        $message = null;
                    }

                    $values = array( 0 => $_POST["name"], 1 => $_POST["description"], 2 => $_POST["driver"], 3 => $_POST["license_plate"], 4 => $_POST["price"], 5 => $_POST["hour_s"], 6 => $_POST["hour_r"]);
                    $data["paradas"] = $this->Rutas_model->preload_data();

                }
                else{
                    $data["paradas"] = $this->Rutas_model->preload_data();
                    $values = array( 0 =>null, 1=>null, 2=>null, 3=>null, 4=>null, 5=>'00:00', 6=>'00:00');
                    $error = array( 0 =>null, 1=>null, 2=>null, 3=>null, 4=>null, 5=>null, 6=>null);
                    $info_stops = null;
                    $message = null;
                }

                $data["info_stops"] = $info_stops;
                $data["values"] = $values;
                $data["error"] = $error;
                $data["menu"] = $menu;
                $data["message"] = $message;
                $this->load->view("admin/template/header", $data);
                $this->load->view("admin/rutas/ingresar_ruta", $data);
                $this->load->view("admin/template/footer", $data);
            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
        
    }

    public function ver_rutas($status = null){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){
                $this->load->model("Rutas_model");
                $data["menu"] = 3;
                $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                $data["id_user"] = $this->session->userdata("id_user");
                $data["title"] = "Gestión de rutas de transporte | Administración Bus Tracking";
                $data["message"] = null;

                $data["table"] = $this->Rutas_model->Ver_rutas();

                $this->load->view("admin/template/header", $data);
                $this->load->view("admin/rutas/ver_rutas", $data);
                $this->load->view("admin/template/footer", $data);

            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }

    public function ver_paradas_de_rutas($id_route){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){
                
                if($id_route != null){
                    $this->load->model("Rutas_model");
                    $data["menu"] = 3;
                    $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                    $data["id_user"] = $this->session->userdata("id_user");
                    $data["title"] = "Gestión de rutas de transporte | Administración Bus Tracking";
                    $data["message"] = null;

                    $data["dataroute"] = $this->Rutas_model->Ver_rutas($id_route);
                    $data["datastops"] = $this->Rutas_model->Ver_paradas_de_rutas($id_route);

                    $this->load->view("admin/template/header", $data);
                    $this->load->view("admin/rutas/ver_paradas_de_rutas", $data);
                    $this->load->view("admin/template/footer", $data);  


                }
                else{
                    redirect("verrutas");
                }

            }
            else{
                redirect("login");
            }
        }else{
            redirect("login");
        }
    }
    
    public function modificar_ruta($id = null){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){
                if($id != null){
                    $this->load->model("Rutas_model");
                    $data["id"] = $id;
                    $menu = 3;
                    $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
                    $data["id_user"] = $this->session->userdata("id_user");
                    $data["title"] = "Gestión de rutas de transporte | Administración Bus Tracking";

                    if(isset($_POST["submit"])){

                        if(isset($_POST["info_stops"]) ){
                            $info_stops = $_POST["info_stops"];
                        }
                        else{
                            $info_stops = null;
                        }

                        if($_POST["name"] != null){

                            if($_POST["description"] != null){

                                if($info_stops != null){
                                    
                                    if($_POST["driver"] != null){

                                        if($_POST["license_plate"] != null){

                                            if($_POST["price"] != null){

                                                if( $_POST["hour_s"] < $_POST["hour_r"] ){

                                                    $values = array( 0 => $_POST["name"], 1 => $_POST["description"], 2 => $_POST["driver"], 3 => $_POST["license_plate"], 4 => $_POST["price"], 5 => $_POST["hour_s"], 6 => $_POST["hour_r"]);
                                                    $error = array( 0 =>null, 1=>null, 2=>null, 3=>null, 4=>null, 5=>null, 6=>null);
                                                    $message = $this->Rutas_model->Modificar_ruta($values, $info_stops, $id);

                                                }
                                                else{
                                                    $error = array(0 => null, 1 => null, 2 => null, 3 => NULL, 4 => null, 5=>'La hora de salida del autobús debe ser menor a la hora de retorno.', 6=>'La hora de retorno del autbús debe ser mayor a la hora de salida');
                                                    $message = null;   
                                                }
       
                                            }
                                            else{
                                                $error = array(0 => null, 1 => null, 2 => null, 3 => NULL, 4 => 'El precio para abordar el autobús es requerido, si está no tiene precio ingresar el valor de $00.00.', 5=>null, 6=>null);
                                                $message = null;    
                                            }

                                        }
                                        else{
                                            $error = array(0 => null, 1 => null, 2 => null, 3 => 'La matricula del autobús es requerida', 4=> null, 5 => null, 6 => null);
                                            $message = null;
                                        }

                                    }
                                    else{
                                        $error = array(0 => null, 1 => null, 2 => 'El nombre del conductor es requerido', 3 => null, 4 => null, 5 => null, 6 => null);
                                        $message = null;
                                    }

                                }
                                else{
                                    $error = array(0 => null, 1 => null, 2 => null, 3 => null, 4=> null, 5 => null, 6 => null);
                                    $message = null;
                                }

                            }
                            else{
                                $error = array(0 => null, 1 => 'La descripción es requerida', 2 => null, 3 => null, 4 => null, 5 => null, 6=>null);
                                $message = null;
                            }

                        }
                        else{
                            $error = array( 0 => 'El nombre es requerido', 1 => null, 2 => null, 3 => null, 4=> null, 5 => null, 6=> null);
                            $message = null;
                        }

                        $values = array( 0 => $_POST["name"], 1 => $_POST["description"], 2 => $_POST["driver"], 3 => $_POST["license_plate"], 4 => $_POST["price"], 5 => $_POST["hour_s"], 6 => $_POST["hour_r"]);
                        $data["paradas"] = $this->Rutas_model->preload_data();

                    }
                    else{
                        $data["paradas"] = $this->Rutas_model->preload_data();
                        $tmp_values = $this->Rutas_model->Ver_rutas($id);
                        $values = array( 0 =>$tmp_values[0]["name"], 1=>$tmp_values[0]["description"], 2=>$tmp_values[0]["driver"], 3=>$tmp_values[0]["license_plate"], 4 => $tmp_values[0]["price"], 5 => $tmp_values[0]["departure_time"], 6 => $tmp_values[0]["return_time"]);

                        $tmp_values = $this->Rutas_model->Ver_paradas_de_rutas($id);
                        $i = 0;
                        foreach ($tmp_values as $row) {
                            $info_stops[$i] = $row["id"]."|".$row["arrival_time"]."|".$row["departure_time"];
                            $i++;
                        }

                        $tmp_values = null;

                        $error = array( 0 =>null, 1=>null, 2=>null, 3=>null, 4=>null, 5=> null, 6=>null);
                        $message = null;
                    }

                    $data["info_stops"] = $info_stops;
                    $data["values"] = $values;
                    $data["error"] = $error;
                    $data["menu"] = $menu;
                    $data["message"] = $message;
                    $this->load->view("admin/template/header", $data);
                    $this->load->view("admin/rutas/editar_rutas", $data);
                    $this->load->view("admin/template/footer", $data);  
                }
                else{
                    redirect("verrutas");
                }
            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }
    

    public function eliminar_ruta(){
        if($this->session->userdata("id_user") != null && $this->session->userdata("username") != null){
            if( $this->session->userdata("logged_in") == TRUE ){
                $id_del = $_POST["id_del"];
                $this->load->model("Rutas_model");
                $message = $this->Rutas_model->Eliminar_ruta($id_del);
                redirect("verrutas");
            }
            else{
                redirect("login");
            }
        }
        else{
            redirect("login");
        }
    }


    /********************************************************/


    /********************************************************
                GESTIÓN DE RUTAS DE AUTOBÚS
    *********************************************************/

    public function datos_generales(){
        $this->load->model("Siteweb_model");
        $message = null;
        $data["menu"] = 4;
        $data["username"] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");
        $data["id_user"] = $this->session->userdata("id_user");
        $data["title"] = "Gestión del sitio web | Administración Bus Tracking";
        $values = $this->Siteweb_model->Obtener_info_website();
        $values2 = $this->Siteweb_model->Obtener_colores();

        if(isset($_POST["submit"])){

            if($_POST["titlepage"] != null){

                if($_POST["phone"] != null){

                    if($_POST["email"] != null){

                        $values = array(0 => $_POST["titlepage"], 1 => $_POST["phone"], 2 => $_POST["email"]);
                        $error = array(0 => null, 1 => null, 2 => null);
                        $message = $this->Siteweb_model->Modificar_info_website($values);

                    }
                    else{
                        $error = array(0 => null, 1 => null, 2 => 'El correo eléctronico de la institución/empresa es requerido.');
                        $values = array(0 => $values[0]["titlepage"], 1 => $values[0]["phone"], 2 => $values[0]["email"]);
                    }

                }
                else{
                    $error = array(0 => null, 1 => 'El telefono o celular de la institución/empresa es requerido.', 2 => null);
                    $values = array(0 => $values[0]["titlepage"], 1 => $values[0]["phone"], 2 => $values[0]["email"]);        
                }

            }
            else{
                $error = array(0 => 'El nombre de la institución/empresa es requerido.', 1 => null, 2 => null);
                $values = array(0 => $values[0]["titlepage"], 1 => $values[0]["phone"], 2 => $values[0]["email"]);
            }

        }
        else{
            $values = array(0 => $values[0]["titlepage"], 1 => $values[0]["phone"], 2 => $values[0]["email"]);
            $error = array(0 => null, 1 => null, 2 => null);
        }

        if(isset($_POST["submit2"])){

            $values2 = array(0 => $_POST["bg_header"], 1 => $_POST["text_header"], 2 => $_POST["bg_body"], 3 => $_POST["bg_footer"], 4 => $_POST["text_footer"], 5 => $_POST["color_text_decoration"]);

            $message = $this->Siteweb_model->Modificar_colores($values2);

        }
        else if(isset($_POST["restablecer"])){
            $values2 = array(0 => $values2[1]["bg_header"], 1 => $values2[1]["text_header"], 2 => $values2[1]["bg_body"], 3 => $values2[1]["bg_footer"], 4 => $values2[1]["text_footer"], 5 => $values2[1]["color_text_decoration"]);

            $message = $this->Siteweb_model->Modificar_colores($values2);
        }
        else{    
            $data["prueba"] = null;
            $values2 = array(0 => $values2[0]["bg_header"], 1 => $values2[0]["text_header"], 2 => $values2[0]["bg_body"], 3 => $values2[0]["bg_footer"], 4 => $values2[0]["text_footer"], 5 => $values2[0]["color_text_decoration"]);
        }

        $data["values"] = $values;
        $data["values2"] = $values2;
        $data["error"] = $error;
        $data["message"] = $message;

        $this->load->view("admin/template/header", $data);
        $this->load->view("admin/siteweb/datos_generales", $data);
        $this->load->view("admin/template/footer", $data);  

    }

    /********************************************************/




    public function login(){
        if(isset($_POST["ingreso"])){
            $username = $_POST["username"];
            $password = $_POST["password"];
            $this->load->model("Login_model");
            $status = $this->Login_model->log_in($username, $password);

            if($status == 1){
                $data["title"] = "Inicio de sesión | Administración Bus Tracking";
                $data["status"] = "El usuario o  la contraseña ingresada es erronea, por favor verificar.";
                $this->load->view("admin/login", $data); 
            }
            else if($status == 0){
                if($this->session->userdata('logged_in') == TRUE){
                    redirect("backend");
                }
            }

        }   
        else{
            $data["title"] = "Inicio de sesión | Administración Bus Tracking";
            $this->load->view("admin/login", $data);    
        }    
    }

    public function loggout(){
        $this->load->model("Login_model");
        $this->Login_model->loggout();
        redirect("backend");
    }




}
