  <!--/ footer Star /-->
  <section class="section-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <center>
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand"><?php echo $website[0]["titlepage"]; ?></h3>
            </div>
            <div class="w-footer-a">
              <ul class="list-unstyled">
                <li class="color-a">
                  <span class="color-text-a">Teléfono: </span> <?php echo $website[0]["phone"]; ?></li>
                <li class="color-a">
                  <span class="color-text-a">Correo eléctronico: </span> <?php echo $website[0]["email"]; ?></li>
              </ul>
            </div>
          </div>
        </center>
        </div>
        
      
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <nav class="nav-footer">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="<?php base_url(); ?>inicio">Inicio</a>
              </li>
              <li class="list-inline-item">
                <a href="<?php base_url(); ?>rutas">Rutas de transporte</a>
              </li>
              <li class="list-inline-item">
                <a href="<?php base_url(); ?>contactanos">Contáctanos</a>
              </li>
            </ul>
          </nav>
          <div class="copyright-footer">
            <p class="copyright color-text-a">
              &copy; Copyright
              <span class="color-a">Bus Tracking </span> | Todos los derechos reservados.
            </p>
          </div>
          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=EstateAgency
            -->
            Diseñado con <a href="https://bootstrapmade.com/">BootstrapMade</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--/ Footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- JavaScript Libraries -->
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/popper/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/easing/easing.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/frontpage/lib/scrollreveal/scrollreveal.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="<?php echo base_url(); ?>assets/frontpage/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php echo base_url(); ?>assets/frontpage/js/main.js"></script>

</body>
</html>
