<?php

/**
 * 
	CLASE PARA LA ADMINISTRACIÓN DE PARADAS DE AUTOBÚS
 */
class Paradas_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
		$this->bustracking = $this->load->database('bustracking', TRUE);
        $this->load->library("session");
	}

	public function Ver_paradas(){
		$sql = "SELECT * FROM bustracking_busstops";
		$query = $this->bustracking->query($sql);

		if( $query->num_rows() > 0 ){
			$data = $query->result_array();
			return $data;
		}
		else{
			return null;
		}
	}

	public function Ingresar_parada($data){
		$sql = "INSERT INTO bustracking_busstops VALUES(null, '".$data[0]."','".$data[1]."','".$data[2]."','".$data[3]."')";

		if($this->bustracking->query($sql) === TRUE){
			return "La parada de autobús ha sido ingresada correctamente.";
		}
		else{
			return "Ha ocurrido un inconveniente al intentar ingresar la parada de autobús, por favor intentarlo de nuevo.";
		}
	}

	public function Eliminar_parada($id){
		$sql = "DELETE FROM bustracking_busstops WHERE id = ". $id;

		if($this->bustracking->query($sql) === TRUE){
			return "El registro se ha eliminado con exito.";
		}
		else{
			return "Ha ocurrido un inconveniente al intentar eliminar la parada de autobús seleccionada, por favor intentarlo de nuevo.";
		}
	}

	public function Obtener_datos_parada($id){
		$sql = "SELECT * FROM bustracking_busstops WHERE id = ". $id;
		$query = $this->bustracking->query($sql);
		
		if( $query->num_rows() > 0 ){
			return $query->result_array();
		}
		else{
			return null;
		}
	}

	public function Modificar_datos_parada($id, $data){
		$sql = "UPDATE bustracking_busstops SET name = '".$data[0]."', description = '".$data[1]."', latitude = '".$data[2]."', lenght = '".$data[3]."' WHERE id = ".$id;

		if($this->bustracking->query($sql) === TRUE){
			return 1;
		}
		else{
			return -1;
		}
	}
}

?>