	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de rutas de autobús <i class="fa fa-angle-right"></i> Ver rutas de autobús</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
                <h1>Lista de rutas de autobús registradas</h1>
                <hr/>
                <a href="<?php echo base_url(); ?>ingresarruta" class="btn btn-primary">Ingresar nueva ruta de autobús</a>
                <br/>
                <br/>
                <div class="content-panel">
                          <h4><i class="fa fa-angle-right"></i> Tabla de datos</h4><hr>
                          
                          <table class="table table-striped table-advance table-hover">  
                            <thead>
                              <tr>
                                  <th> Nombre de ruta de autobús</th>
                                  <th> Descripción</th>
                                  <th> Conductor</th>
                                  <th> Matricula de vehiculo</th>
                                  <th><i class=" fa fa-edit"></i> Acciones</th>
                              </tr>
                            </thead>
                              <?php 
                                if($table == null){
                              ?>
                                  <tbody>
                                    <tr>
                                      <td colspan="5">No hay datos que mostrar...</td>
                                    </tr>
                                  </tbody>                                

                              <?php
                                }
                                else{
                              ?>
                                  <tbody>
                                    <?php 
                                      foreach ($table as $row) {
                                        echo "<tr>";
                                        echo "<td>".$row["name"]."</td>";
                                        echo "<td>".$row["description"]."</td>";
                                        echo "<td>".$row["driver"]."</td>";
                                        echo "<td>".$row["license_plate"]."</td>";
                                        echo '<td>
                                                <a class="btn btn-primary btn-xs" href="'.base_url().'verdetallederutas/'.$row["id"].'"><i class="fa fa-search"></i> Ver paradas de autobús</a><br/><br/>
                                                <a class="btn btn-primary btn-xs" href="'.base_url().'modificarruta/'.$row["id"].'"><i class="fa fa-pencil"></i></a>
                                                <button onClick="eliminar('.$row["id"].')" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                            </td>';
                                        echo "</tr>";
                                      }


                                    ?>
                                  </tbody>
                              <?php
                                }
                              ?>

                          </table>
                      </div>



          		</div>
          	</div>
			
		      </section>
	     </section><!-- /MAIN CONTENT -->

       <style type="text/css">
         #modal_eliminar{
          display: none;
          position: absolute;
          z-index: 999;
          width: 20%;
          height: auto;
          top: 40%;
          left: 40%;
          padding: 1%;
          box-shadow: 1px 1px 10px;
          text-align: center;
          background: white;
         }
       </style>

       <div id="modal_eliminar">
         <h3>¿Esta seguro que desea eliminar el registro?</h3>
         <form method="POST" action="<?php echo base_url(); ?>eliminarruta">
           <input type="hidden" name="id_del" id="id_del" value="">
           <input type="submit" class="form-control btn btn-danger" name="eliminar" value="Aceptar" >
         </form>
         <br/>
         <button id="cancelar" onclick="cancelar()" class="form-control btn btn-default"> Cancelar</button>
       </div>


       <script type="text/javascript">
         
        function eliminar(id){
          document.getElementById("modal_eliminar").style.display = "block";
          $("#id_del").val(id);
        }

        function cancelar(){
          document.getElementById("modal_eliminar").style.display = 'none';
          $("#id_del").val() = "";
        }

       </script>
	  
