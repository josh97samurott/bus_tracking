<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


/****ADMINISTRACIÓN DEL SITIO*************/
$route["backend"] = 'administracion/index';
$route["login"] = 'administracion/login';
$route["loggout"] = 'administracion/loggout';

$route["verparadas"] = 'administracion/ver_paradas';
$route["verparadas/(:any)"] = 'administracion/ver_paradas/$1';
$route["ingresarparada"] = 'administracion/ingresar_parada';
$route["modificarparada/(:any)"] = 'administracion/modificar_parada/$1';
$route["eliminarparada"] = 'administracion/eliminar_parada';

$route["verrutas"] = 'administracion/ver_rutas';
$route["verrutas/(:any)"] = 'administracion/ver_rutas/$1';
$route["verdetallederutas/(:any)"] = 'administracion/ver_paradas_de_rutas/$1';
$route["ingresarruta"] = 'administracion/ingresar_ruta';
$route["modificarruta/(:any)"] = 'administracion/modificar_ruta/$1';
$route["eliminarruta"] = 'administracion/eliminar_ruta';
$route["infouser/(:any)"] = 'administracion/modificarusuario/$1';

$route["datosgenerales"] = 'administracion/datos_generales';



/****FRONTPAGE DEL SITIO******************/
$route['default_controller'] = 'frontpage/index';
$route['inicio'] = 'frontpage/index';
$route['ruta/(:any)'] = 'frontpage/detalle_ruta/$1';
$route['rutas'] = 'frontpage/rutas';
$route['buscador'] = 'frontpage/buscar';
$route['contactanos'] = 'frontpage/contactform';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
