	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de paradas de autobús <i class="fa fa-angle-right"></i> Agregar parada de autobús</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <h1>Ingresar una nueva parada de autobús</h1>
                <hr/>
                <form action="<?php echo base_url(); ?>ingresarparada" method="POST">
                  
                  <div class="col-lg-12">
                    <div class="showback">
                      <p>
                        <h3>Indicaciones:</h3>
                        Haga clic en el lugar del mapa donde desea agregar una nueva parada de autobús, automaticamente las coordenadas de latitud y longitud se agregarán a los campos respectivos del formulario. También, puede agregar las coordenadas manualmente.<br/>
                        La latitud y longitud (coordenadas) puede encontrarla en la descripción de un lugar buscado en Google Maps.<br/>
                        Para más información consulte el siguiente enlace: <a href="https://support.google.com/maps/answer/3092445?hl=es&ref_topic=3092444" target="blank">Buscar ubicaciones en Google Maps.</a>
                      </p>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <label>Ingrese un nombre de identificación:</label>
                      <input type="text" name="name" class="form-control" value="<?php echo $values[0]; ?>" />
                      <?php if($error[0] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[0]."</b> </div>"; 
                      } ?>

                      <br/>
                      <label>Ingrese una descripción:</label>
                      <textarea class="form-control" name="description"><?php echo $values[1]; ?></textarea>
                      <?php if($error[1] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[1]."</b> </div>"; 
                      } ?>

                      <br/>
                      <label>Ingrese las coordenadas de la parada de autobús:</label>
                      <input type="text" name="latitude" id="latitude" class="form-control" placeholder="Latitud" value="<?php echo $values[2]; ?>" />
                      <?php if($error[2] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[2]."</b> </div>"; 
                      } ?>

                      <br/>
                      <input type="text" name="lenght" id="lenght" class="form-control" placeholder="Longitud" value="<?php echo $values[3]; ?>" />
                      <?php if($error[3] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[3]."</b> </div>"; 
                      } ?>

                    </div>
                  </div>
                  
                  
                  <div class="col-lg-6">
                    <div class="showback">
                      <style>
                        /* Always set the map height explicitly to define the size of the div
                         * element that contains the map. */
                        #map {
                          min-height: 400px;
                        }
                        /* Optional: Makes the sample page fill the window. */
                        html, body {
                          height: 100%;
                          margin: 0;
                          padding: 0;
                        }
                      </style>
                      <div id="map"></div>
                      <script>
                        // In the following example, markers appear when the user clicks on the map.
                        // The markers are stored in an array.
                        // The user can then click an option to hide, show or delete the markers.
                        var map;
                        var markers = [];

                        function initMap() {
                          var haightAshbury = {lat: 13.8333000, lng: -88.9167000};

                          map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 6,
                            center: {lat: 13.8333000, lng: -88.9167000},
                            zoomControl: false,
                            scaleControl: true
                          });

                          // This event listener will call addMarker() when the map is clicked.
                          map.addListener('click', function(event) {
                            addMarker(event.latLng);
                            setMapOnAll(map);
                          });

                          // Adds a marker at the center of the map.
                         // addMarker(haightAshbury);
                        }

                        // Adds a marker to the map and push to the array.
                        function addMarker(location) {
                          deleteMarkers();
                          var marker = new google.maps.Marker({
                            position: location,
                            map: map
                          });
                          markers.push(marker);
                        }

                        // Sets the map on all markers in the array.
                        function setMapOnAll(map) {
                          for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);

                            var latitude = markers[i].position;
                            var lenght = markers[i].position;
                            
                            latitude = String(latitude);
                            latitude = latitude.split(",", 1);
                            latitude = String(latitude);
                            latitude = latitude.substr(1);
                            
                            lenght = String(lenght);
                            lenght = lenght.split(",", 2);
                            lenght = String(lenght[1]);
                            lenght = lenght.split(")", 1);
                            lenght = String(lenght).trim();


                            document.getElementById("latitude").value = latitude;
                            document.getElementById("lenght").value = lenght;
                          }
                        }

                        // Removes the markers from the map, but keeps them in the array.
                        function clearMarkers() {
                          setMapOnAll(null);
                        }

                        // Shows any markers currently in the array.
                        function showMarkers() {
                          setMapOnAll(map);
                        }

                        // Deletes all markers in the array by removing references to them.
                        function deleteMarkers() {
                          clearMarkers();
                          markers = [];
                        }

                      </script>
                      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwl5ElPJ8SSyB1Pg_pEse-smlLIOj4Szk&callback=initMap" async defer></script>
                      <!--img src="<?php echo base_url(); ?>/assets/img/admin/latitudylongitud.png" width="100%"/--> 
                    </div>
                  </div>
                  <br/>
                  <br/>

                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="Ingresar parada de autobús" />
                    </div>
                  </div>
                  
                  <div class="col-lg-6">
                    <div class="showback">
                      <a href="<?php echo base_url(); ?>verparadas" class="btn btn-default btn-lg btn-block">Cancelar</a>
                    </div>
                  </div>
                  <br/>
                
                </form>

          		</div>
          	</div>
			 
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->

       