      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de rutas de autobús <i class="fa fa-angle-right"></i> Ver rutas de autobús</h3>
          	<div class="row mt">
              <div class="col-lg-12">
            		<h1>Detalle de ruta de autobús</h1>
                <hr/>
                <div class="col-lg-12">
                  <div class="form-panel">
                    <a class="btn btn-default" href="<?php echo base_url(); ?>verrutas"><i class="fa fa-arrow-left"></i> Regresar</a>
                    <br/><br/>
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Información de la ruta de autobús</h4>
                    <div class="form-horizontal style-form">
                    <?php
                      foreach ($dataroute as $row) {
                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Nombre de la ruta: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["name"].'" disabled></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Descripción: </label><div class="col-sm-10">';
                        echo '<textarea class="form-control" disabled>'.$row["description"].'</textarea></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Conductor: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["driver"].'" disabled></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Matrícula de autobús: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["license_plate"].'" disabled></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Tarifa para abordar: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["price"].'" disabled></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Hora de salida de la ruta de autobaús: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["departure_time"].'" disabled></div></div>';

                        echo '<div class="form-group"><label class="col-sm-2 col-sm-2 control-label">Hora de retorno de la ruta de autobús: </label><div class="col-sm-10">';
                        echo '<input type="text" class="form-control" value="'.$row["return_time"].'" disabled></div></div>';
                      }
                      
                    ?>
                    </div>

                    <br/><h4 class="mb"><i class="fa fa-angle-right"></i> Paradas que realiza la ruta de autobús</h4>


                    <div id="map"></div>
                    <style>
                        /* Always set the map height explicitly to define the size of the div
                         * element that contains the map. */
                        #map {
                          min-height: 400px;
                        }
                        /* Optional: Makes the sample page fill the window. */
                        html, body {
                          height: 100%;
                          margin: 0;
                          padding: 0;
                        }
                    </style>

                    <script>
                        // In the following example, markers appear when the user clicks on the map.
                        // The markers are stored in an array.
                        // The user can then click an option to hide, show or delete the markers.
                        function initMap() {
                          
                          var place = {lat: <?php echo $datastops[0]["latitude"]; ?>, lng: <?php echo $datastops[0]["lenght"]; ?>};

                          var map = new google.maps.Map(
                              document.getElementById('map'), {zoom: 10, center: place});

                          <?php 
                            $i = 0;
                            foreach ($datastops as $row) { ?>
                            var place = {lat: <?php echo $row["latitude"]; ?>, lng: <?php echo $row["lenght"]; ?>};
                            
                            var marker<?php echo $i; ?> = new google.maps.Marker({position: place, title: "<?php echo $row['name'] . ' ('.$row['departure_time'].' - '.$row['arrival_time'].')' ?>", map: map});

                            var contentString<?php echo $i; ?> = '<div id="content<?php echo $i; ?>">'+
                                                    '<h5 id="firstHeading" class="firstHeading"><?php echo $row["name"]; ?></h5>'+
                                                      '<div style="float:right; width:100%;margin-top: 0px;">'+
                                                        '<p>Hora de llegada: <?php echo $row["arrival_time"]; ?> <br/>'+
                                                        'Hora de salida: <?php echo $row["departure_time"]; ?> <br/>'+
                                                        '<b> Formato en hora militar </b></p>'+
                                                      '</div>'+
                                                 '</div>';
                                                 
                            var infowindow<?php echo $i; ?> = new google.maps.InfoWindow({
                              content: contentString<?php echo $i; ?>
                            });               

                            google.maps.event.addListener(marker<?php echo $i; ?>, 'click', function() {
                              infowindow<?php echo $i; ?>.open(map,marker<?php echo $i; ?>);
                            });

                            infowindow<?php echo $i; ?>.open(map,marker<?php echo $i; ?>);

                            marker<?php echo $i; ?>.setMap(map);
                          <?php $i++; } ?>

                        }

                      </script>
                      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwl5ElPJ8SSyB1Pg_pEse-smlLIOj4Szk&callback=initMap" async defer></script>
                    
                  </div>
            		</div>
              </div>
          	</div>
			 
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->
