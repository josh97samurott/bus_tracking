  <!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single">Rutas de transporte disponibles</h1>
            <!--span class="color-text-a">Grid Properties</span-->
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="<?php echo base_url(); ?>inicio">Inicio</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                Rutas de transporte
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ Property Grid Star /-->
  <section class="property-grid grid">
    <div class="container">
      <div class="row">

        <?php 
        if($rutas != null) {
        foreach ($rutas as $row) { 
        ?>
        
        <div class="col-md-4">
          <div class="card-box-a card-shadow">
            <div class="img-box-a">
              <img src="<?php echo base_url(); ?>assets/frontpage/img/property-6.jpg" alt="" class="img-a img-fluid">
            </div>
            <div class="card-overlay">
              <div class="card-overlay-a-content">
                <div class="card-header-a">
                  <h2 class="card-title-a">
                    <a href="#"><?php echo $row["name"]; ?></a>
                  </h2>
                </div>
                <div class="card-body-a">
                  <div class="price-box d-flex">
                    <span class="price-a">Precio | $ <?php echo $row["price"]; ?></span>
                  </div>
                  <a href="<?php echo base_url(); ?>ruta/<?php echo $row["id"]; ?>" class="link-a">Clic para más detalles
                    <span class="ion-ios-arrow-forward"></span>
                  </a>
                </div>
                <div class="card-footer-a">
                  <ul class="card-info d-flex justify-content-around">
                    <li>
                      <h4 class="card-info-title">Hora de salida</h4>
                      <span><?php echo $row["departure_time"]; ?></span>
                    </li>
                    <li>
                      <h4 class="card-info-title">Hora de retorno</h4>
                      <span><?php echo $row["return_time"]; ?></span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>


        <?php } 
        } else{
          echo "<h6>No se encontraron registros de rutas de transporte...</h6>";
        }?>
        
      </div>
    </div>
  </section>
  <!--/ Property Grid End /-->