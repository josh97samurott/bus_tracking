	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de del sitio web público </h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <h1>Editar datos del sitio público</h1>
                <hr/>
                <form action="<?php echo base_url(); ?>datosgenerales" method="POST">

                  <div class="col-lg-12">
                    <div class="showback">
                      <h4>Datos generales</h4><hr/>
                      <label>Nombre de la institución/empresa:</label>
                      <input type="text" name="titlepage" class="form-control" value="<?php echo $values[0]; ?>" />
                      <?php if($error[0] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[0]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Telefono o celular de la institución/empresa:</label>
                      <input type="text" class="form-control" name="phone" value="<?php echo $values[1]; ?>" />
                      <?php if($error[1] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[1]."</b> </div>"; 
                      } ?>

                      <br/>                      

                      <label>Correo eléctronico de la institución/empresa:</label>
                      <input type="text" class="form-control" name="email" value="<?php echo $values[2]; ?>" />
                      <?php if($error[2] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[2]."</b> </div>"; 
                      } ?>
                      
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="Editar datos generales" />
                    </div>
                  </div>
                  
                  <br/>
                
                </form>

                <form action="<?php echo base_url(); ?>datosgenerales" method="POST">
                  <div class="col-lg-12">
                    <div class="showback">
                      <h4>Personalización de colores</h4><hr/>
                      <label>Color de fondo del menú:</label>
                      <input type="color" class="form-control" style="width: 200px;" name="bg_header" value="<?php echo $values2[0]; ?>" />
                      <br/><br/>

                      <label>Color de texto del menú:</label>
                      <input type="color" class="form-control" style="width: 200px;" name="text_header" value="<?php echo $values2[1]; ?>" />
                      <br/><br/>                 

                      <label>Color de fondo de la página</label>
                      <input type="color" class="form-control" style="width: 200px;" name="bg_body" value="<?php echo $values2[2]; ?>" />
                      <br/><br/>

                      <label>Color de fondo del pie de página:</label>
                      <input type="color" class="form-control" style="width: 200px;" name="bg_footer" value="<?php echo $values2[3]; ?>" />
                      <br/><br/>

                      <label>Color de texto del pie de página:</label>
                      <input type="color" class="form-control" style="width: 200px;" name="text_footer" value="<?php echo $values2[4]; ?>" />
                      <br/> <br/>

                      <label>Color complementario:</label>
                      <input type="color" class="form-control" style="width: 200px;" name="color_text_decoration" value="<?php echo $values2[5]; ?>" />
                      
                      
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="submit2" class="btn btn-primary btn-lg btn-block" value="Editar colores del sitio" />
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="restablecer" class="btn btn-success btn-lg btn-block" value="Restablecer por defecto" />
                    </div>
                  </div>
                  
                  <br/>

                </form>

          		</div>
          	</div>
			 
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->


    