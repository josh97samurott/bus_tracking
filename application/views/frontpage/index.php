<!--/ Carousel Star /-->
  <div class="intro intro-carousel">
    <div id="carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-a intro-item bg-image" style="background-image: url(<?php echo base_url(); ?>assets/frontpage/img/slide-1.jpg)">
        <div class="overlay overlay-a"></div>
        <div class="intro-content display-table">
          <div class="table-cell">
            <div class="container">
              <div class="row">
                <div class="col-lg-8">
                  <div class="intro-body">
                    <p class="intro-title-top"><?php echo $website[0]["titlepage"]; ?>
                      <br> <?php echo $website[0]["phone"]; ?></p>
                    <h1 class="intro-title mb-4">
                      <span class="color-b">SERVICIO </span> DE
                      <br> TRANSPORTE</h1>
                    <!--p class="intro-subtitle intro-price">
                      <a href="#"><span class="price-a">rent | $ 12.000</span></a>
                    </p-->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!--/ Carousel end /-->

<!--/ Property Star /-->
  <section class="section-property section-t8">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-wrap d-flex justify-content-between">
            <div class="title-box">
              <h2 class="title-a">Rutas de transporte</h2>
            </div>
            <div class="title-link">
              <a href="<?php echo base_url(); ?>rutas">Ver todas
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div id="property-carousel" class="owl-carousel owl-theme">

      <?php 
      if($rutas != null){
      foreach ($rutas as $row) { 
      ?>
      
        <div class="carousel-item-b">
          <div class="card-box-a card-shadow">
            <div class="img-box-a">
              <img src="<?php echo base_url(); ?>assets/frontpage/img/property-6.jpg" alt="" class="img-a img-fluid">
            </div>
            <div class="card-overlay">
              <div class="card-overlay-a-content">
                <div class="card-header-a">
                  <h2 class="card-title-a">
                    <a href="<?php echo base_url(); ?>ruta/<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></a>
                  </h2>
                </div>
                <div class="card-body-a">
                  <div class="price-box d-flex">
                    <span class="price-a">Precio | $ <?php echo $row["price"]; ?></span>
                  </div>
                  <a href="<?php echo base_url(); ?>ruta/<?php echo $row["id"]; ?>" class="link-a">Clic para más detalles
                    <span class="ion-ios-arrow-forward"></span>
                  </a>
                </div>
                <div class="card-footer-a">
                  <ul class="card-info d-flex justify-content-around">
                    <li>
                      <h4 class="card-info-title">Hora de Salida</h4>
                      <span><?php echo $row["departure_time"]; ?></span>
                    </li>
                    <li>
                      <h4 class="card-info-title">Hora de Retorno</h4>
                      <span><?php echo $row["return_time"]; ?></span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

      <?php } 
      }else{
        echo "<h6>No se encontraron registros de rutas de transporte...</h6>";
      }?>

      </div>
    </div>
  </section>
  <!--/ Property End /-->
  <br/>


