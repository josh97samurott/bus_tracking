	  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Gestión de rutas de autobús <i class="fa fa-angle-right"></i> Editar ruta de autobús</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <h1>Editar ruta de autobús</h1>
                <hr/>
                <form action="<?php echo base_url(); ?>modificarruta/<?php echo $id ?>" method="POST">

                  <div class="col-lg-12">
                    <div class="showback">
                      <label>Ingrese un nombre para la ruta de autobús:</label>
                      <input type="text" name="name" class="form-control" value="<?php echo $values[0]; ?>" />
                      <?php if($error[0] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[0]."</b> </div>"; 
                      } ?>

                      <br/>
                      <label>Ingrese una descripción:</label>
                      <textarea class="form-control" name="description"><?php echo $values[1]; ?></textarea>
                      <?php if($error[1] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[1]."</b> </div>"; 
                      } ?>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <label>Seleccione una parada de autobús:</label>
                      <select class="form-control" id="stops">
                        <?php 
                        foreach ($paradas as $fila) {
                          echo "<option value='".$fila["id"]."' id='".$fila["id"]."'>".$fila["name"]."</option>";
                        }
                        ?>
                      </select>
                      <br/>
                      
                      <label>Ingrese la hora de llegada: </label>
                      <input type="time" class="form-control" id="arrival_time" value="00:00" required="false" />
                      <br/>
                      
                      <label>Ingrese la hora de salida: </label>
                      <input type="time" class="form-control" id="departure_time" value="00:00" required="false"/>
                      <div class="alert alert-danger" style="display: none;"><b>Debe de ingresar la hora de salida</b></div>

                      <br/>
                      
                      <!--label>Día: </label>
                      <select class="form-control" id="day">
                        <option value="1" selected>Lunes</option>
                        <option value="2">Martes</option>
                        <option value="3">Miércoles</option>
                        <option value="4">Jueves</option>
                        <option value="5">Viernes</option>
                        <option value="6">Sábado</option>
                        <option value="7">Domingo</option>
                      </select-->
                      <br/>
                      <a href="#" class="btn btn-success" onclick="agregarParada()">Agregar parada de autobús</a>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="showback">
                      <h4>Paradas de autobús por las que transita la ruta de transporte:</h4>

                      <select multiple class="form-control" style="height: 243px;" name="info_stops[]" id="info_stops">
                      </select>
                      <?php 
                          if($info_stops != null){
                            foreach ($info_stops as $row) {
                      
                              $string = explode("|", $row);
                              echo "<script type='text/javascript'>
                                var textoption = document.getElementById('".$string[0]."').textContent;
                                var optioninfostops = document.createElement('option');
                                optioninfostops.appendChild(document.createTextNode(textoption + ' (HLL: '+'".$string[1]."' + ' - HS: '+ '".$string[2]."' + ')'));
                                optioninfostops.value = '".$row."';
                                optioninfostops.selected = true;
                                document.getElementById('info_stops').appendChild(optioninfostops);
                              </script>";
                            }
                          }
                          else{
                            echo "<br/><div class='alert alert-danger' > <b> Debe de ingresar al menos una para de autobús </b> </div>";
                          }
                      ?>
                      
                      <br/>
                      <a href="#" class="btn btn-danger" onclick="quitarParadas()">Quitar parada de autobús seleccionada</a>
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="showback">
                      <label>Ingrese el nombre del conductor:</label>
                      <input type="text" name="driver" id="driver" class="form-control" placeholder="Conductor" value="<?php echo $values[2]; ?>" />
                      <?php if($error[2] != null){
                        echo "<br/><div class='alert alert-danger'> <b>".$error[2]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Ingrese el número de placa del autobús:</label>
                      <input type="text" name="license_plate" id="license_plate" class="form-control" placeholder="Número de placa" value="<?php echo $values[3]; ?>" />
                      <?php if($error[3] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[3]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Precio de la ruta de autobús *:</label>
                      <input type="number" min="00.00" step="0.01" max="10000.00" name="price" id="price" class="form-control" placeholder="00.00" value="<?php echo $values[4]; ?>" />
                      <?php if($error[4] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[4]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Hora de salida de la ruta de autobús *:</label>
                      <input type="time" name="hour_s" id="hour_s" class="form-control" placeholder="Hora de salida" value="<?php echo $values[5]; ?>" />
                      <?php if($error[5] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[5]."</b> </div>"; 
                      } ?>

                      <br/>

                      <label>Hora de retorno de la ruta de autobús *:</label>
                      <input type="time" name="hour_r" id="hour_r" class="form-control" placeholder="Hora de retorno" value="<?php echo $values[6]; ?>" />
                      <?php if($error[6] != null){ 
                        echo "<br/><div class='alert alert-danger'> <b>".$error[6]."</b> </div>"; 
                      } ?>
                    </div>
                  </div>
            
                  <div class="col-lg-6">
                    <div class="showback">
                      <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="Editar ruta de autobús" />
                    </div>
                  </div>
                  
                  <div class="col-lg-6">
                    <div class="showback">
                      <a href="<?php echo base_url(); ?>verrutas" class="btn btn-default btn-lg btn-block">Cancelar</a>
                    </div>
                  </div>
                  <br/>
                
                </form>

          		</div>
          	</div>
			 
		      </section><! --/wrapper -->
	     </section><!-- /MAIN CONTENT -->


       <script type="text/javascript">
        function agregarParada(){
          var stops_id = document.getElementById("stops").value;
          var stops_text = document.getElementById(""+stops_id+"").textContent;
          var departure_time = document.getElementById("departure_time").value;
          var arrival_time = document.getElementById("arrival_time").value;
          //var day = document.getElementById("day").value;
          var string_id = stops_id+"|"+arrival_time+"|"+departure_time;
          var string_text = stops_text + " (HLL: "+arrival_time+" - HS: "+departure_time+")";

          var o = new Option(string_text, string_id);
          $("#info_stops").append(o);
          selectedAll();
        }

        function quitarParadas(){
          $("#info_stops option:selected").remove();
        }

        function selectedAll(){
          selectBox = document.getElementById("info_stops");

          for (var i = 0; i < selectBox.options.length; i++) 
          { 
               selectBox.options[i].selected = true; 
          } 
        }
      </script>