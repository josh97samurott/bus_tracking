  <?php 
  if($ruta == null){
    redirect(base_url().'inicio');
  }
  ?>
  <!--/ Intro Single star /-->
  <section class="intro-single">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-8">
          <div class="title-single-box">
            <h1 class="title-single"><?php echo $ruta[0]["name"]; ?></h1>
          </div>
        </div>
        <div class="col-md-12 col-lg-4">
          <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="index.html">Inicio</a>
              </li>
              <li class="breadcrumb-item">
                <a href="property-grid.html">Rutas de transporte</a>
              </li>
              <li class="breadcrumb-item active" aria-current="page">
                <?php echo $ruta[0]["name"]; ?>
              </li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </section>
  <!--/ Intro Single End /-->

  <!--/ Property Single Star /-->
  <section class="property-single nav-arrow-b">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="row justify-content-between">
            <div class="col-md-5 col-lg-4">
              <div class="property-price d-flex justify-content-center foo">
                <div class="card-header-c d-flex">
                  <div class="card-box-ico">
                    <span class="ion-money">$</span>
                  </div>
                  <div class="card-title-c align-self-center">
                    <h5 class="title-c"><?php echo $ruta[0]["price"] ?></h5>
                  </div>
                </div>
              </div>
              <div class="property-summary">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="title-box-d section-t4">
                      <h3 class="title-d">Información general</h3>
                    </div>
                  </div>
                </div>
                <div class="summary-list">
                  <ul class="list">
                    <li class="d-flex justify-content-between">
                      <span>Formaro en hora militar</span>
                    </li>
                    <li class="d-flex justify-content-between">
                      <strong>Hora de salida:</strong>
                      <span><?php echo $ruta[0]["departure_time"] ?></span>
                    </li>
                    <li class="d-flex justify-content-between">
                      <strong>Hora de retorno:</strong>
                      <span><?php echo $ruta[0]["return_time"] ?></span>
                    </li>
                    <li class="d-flex justify-content-between">
                      <strong>Conductor:</strong>
                      <span><?php echo $ruta[0]["driver"]; ?></span>
                    </li>
                    <li class="d-flex justify-content-between">
                      <strong>Matrícula:</strong>
                      <span><?php echo $ruta[0]["license_plate"]; ?></span>
                    </li>
              
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-md-7 col-lg-7 section-md-t3">
              <div class="row">
                <div class="col-sm-12">
                  <div class="title-box-d">
                    <h3 class="title-d">Descripción</h3>
                  </div>
                </div>
              </div>
              <div class="property-description">
                <p class="description color-text-a"><?php echo $ruta[0]["description"]; ?></p>
              </div>
              <div class="row section-t3">
                <div class="col-sm-12">
                  <div class="title-box-d">
                    <h3 class="title-d">Paradas del autobús</h3>
                  </div>
                </div>
              </div>
              <div class="amenities-list color-text-a">
                <ul class="list-a no-margin">
                  <?php foreach ($paradas as $row) { ?>
                    <li><?php echo $row["name"]; ?></li>
                  <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-10 offset-md-1">
          <ul class="nav nav-pills-a nav-pills mb-3 section-t3" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-map-tab" data-toggle="pill" href="#pills-map" role="tab" aria-controls="pills-map"
                aria-selected="false">Paradas del autobús ubicadas geográficamente</a>
            </li>
          </ul>
          <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-map" role="tabpanel" aria-labelledby="pills-map-tab">

              <div id="map"></div>
              <style>
                /* Always set the map height explicitly to define the size of the div
                * element that contains the map. */
                #map {
                  min-height: 460px;
                }
                /* Optional: Makes the sample page fill the window. */
                html, body {
                  height: 100%;
                  margin: 0;
                  padding: 0;
                  border: 0;
                }
              </style>

              <script>
                // In the following example, markers appear when the user clicks on the map.
                // The markers are stored in an array.
                // The user can then click an option to hide, show or delete the markers.
                function initMap() {
                            
                  var place = {lat: <?php echo $paradas[0]["latitude"]; ?>, lng: <?php echo $paradas[0]["lenght"]; ?>};

                  var map = new google.maps.Map(
                    document.getElementById('map'), {zoom: 12, center: place});

                  <?php 
                    $i = 0;
                    foreach ($paradas as $row) { ?>
                      var place = {lat: <?php echo $row["latitude"]; ?>, lng: <?php echo $row["lenght"]; ?>};
                            
                      var marker<?php echo $i; ?> = new google.maps.Marker({position: place, title: "<?php echo $row['name'] . ' ('.$row['departure_time'].' - '.$row['arrival_time'].')' ?>", map: map});

                      var contentString<?php echo $i; ?> = '<div id="content<?php echo $i; ?>">'+
                                                    '<h5 id="firstHeading" class="firstHeading"><?php echo $row["name"]; ?></h5>'+
                                                      '<div style="float:right; width:100%;margin-top: 0px;">'+
                                                        '<p>Hora de llegada: <?php echo $row["arrival_time"]; ?> <br/>'+
                                                        'Hora de salida: <?php echo $row["departure_time"]; ?> <br/>'+
                                                        '<b> Formato en hora militar </b></p>'+
                                                      '</div>'+
                                                 '</div>';
                                                 
                      var infowindow<?php echo $i; ?> = new google.maps.InfoWindow({
                        content: contentString<?php echo $i; ?>
                      });               

                      google.maps.event.addListener(marker<?php echo $i; ?>, 'click', function() {
                        infowindow<?php echo $i; ?>.open(map,marker<?php echo $i; ?>);
                      });

                      infowindow<?php echo $i; ?>.open(map,marker<?php echo $i; ?>);

                      marker<?php echo $i; ?>.setMap(map);
                  <?php $i++; } ?>

                }

              </script>
              <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwl5ElPJ8SSyB1Pg_pEse-smlLIOj4Szk&callback=initMap" async defer></script>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Property Single End /-->
  <br/><br/>
  <!--/ News Single Star /-->
  <section class="news-single nav-arrow-b">
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1 col-lg-10 offset-lg-1">
          <div class="title-box-d">
            <h3 class="title-d">Comentarios</h3>
          </div>
          <div class="box-comments">
            <ul class="list-comments">

              <?php 
              if($comments != null){
                foreach ($comments as $row) { 
              ?>

                <li>
                  <div class="comment-avatar">
                    <img src="img/author-2.jpg" alt="">
                  </div>
                  <div class="comment-details">
                    <h4 class="comment-author">Anónimo</h4>
                    <span><?php echo $row["public_date"]; ?></span>
                    <p class="comment-description">
                      <?php echo $row["comment"]; ?>
                    </p>
                  </div>
                </li>  
              
              <?php 
                }
              }
              else{
                echo "<li> <div class='comment-details'> <h4 class='comment-author'>No hay comentarios...</h4> </div>";
              } 
              ?>
              
            </ul>
          </div>
          <div class="form-comments">
            <div class="title-box-d">
              <h3 class="title-d"> Deja un comentario anónimo</h3>
            </div>
            <form class="form-a" action="<?php echo base_url(); ?>ruta/<?php echo $ruta[0]["id"]; ?>" method="POST">
              <div class="row">
                <div class="col-md-12 mb-3">
                  <div class="form-group">

                    <label for="textMessage">Ingresa el comentario</label><?php if($error != null){ echo " &nbsp;&nbsp;<label class='badge badge-danger'>".$error."</label>"; } ?>
                    <?php if($comment_status != null){ echo " &nbsp;&nbsp;".$comment_status; } ?>
                    <textarea id="textMessage" name="textMessage" class="form-control" placeholder="Comentario *" cols="45"
                      rows="8" required></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <input type="submit" name="submit" class="btn btn-a" value="Publicar Comentario" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ News Single End /-->