-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-11-2019 a las 05:38:53
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bustracking`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_busroutes`
--

DROP TABLE IF EXISTS `bustracking_busroutes`;
CREATE TABLE IF NOT EXISTS `bustracking_busroutes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `driver` text NOT NULL,
  `license_plate` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `departure_time` varchar(10) NOT NULL,
  `return_time` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_busstops`
--

DROP TABLE IF EXISTS `bustracking_busstops`;
CREATE TABLE IF NOT EXISTS `bustracking_busstops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `lenght` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_comments`
--

DROP TABLE IF EXISTS `bustracking_comments`;
CREATE TABLE IF NOT EXISTS `bustracking_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_routes` int(11) NOT NULL,
  `comment` text NOT NULL,
  `public_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bustracking_comments`
--

INSERT INTO `bustracking_comments` (`id`, `id_routes`, `comment`, `public_date`) VALUES
(1, 9, 'asdsadsa', '2019-11-07'),
(2, 9, 'sdasd', '2019-11-07'),
(3, 9, 'No me gusta esta ruta de transporte.. me robaron en el bus >:(', '2019-11-07'),
(4, 11, 'Ahí me robaron.', '2019-11-08'),
(5, 14, 'Ahí me robaron no se suban en esa ruta.', '2019-11-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_frontpagecolors`
--

DROP TABLE IF EXISTS `bustracking_frontpagecolors`;
CREATE TABLE IF NOT EXISTS `bustracking_frontpagecolors` (
  `id` int(11) NOT NULL,
  `bg_header` varchar(7) NOT NULL,
  `text_header` varchar(7) NOT NULL,
  `bg_body` varchar(7) NOT NULL,
  `bg_footer` varchar(7) NOT NULL,
  `text_footer` varchar(7) NOT NULL,
  `color_text_decoration` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bustracking_frontpagecolors`
--

INSERT INTO `bustracking_frontpagecolors` (`id`, `bg_header`, `text_header`, `bg_body`, `bg_footer`, `text_footer`, `color_text_decoration`) VALUES
(0, '#ffd777', '#ffffff', '#f2f2f2', '#424a5d', '#ffffff', '#68dff0'),
(1, '#ffd777', '#ffffff', '#f2f2f2', '#424a5d', '#ffffff', '#68dff0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_frontpagedata`
--

DROP TABLE IF EXISTS `bustracking_frontpagedata`;
CREATE TABLE IF NOT EXISTS `bustracking_frontpagedata` (
  `id` int(11) NOT NULL,
  `titlepage` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bustracking_frontpagedata`
--

INSERT INTO `bustracking_frontpagedata` (`id`, `titlepage`, `phone`, `email`) VALUES
(0, 'Nombre Empresa', '(000) 000 000', 'correo@correo.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_routes_rel_stops`
--

DROP TABLE IF EXISTS `bustracking_routes_rel_stops`;
CREATE TABLE IF NOT EXISTS `bustracking_routes_rel_stops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_routes` int(11) NOT NULL,
  `id_stops` int(11) NOT NULL,
  `arrival_time` varchar(10) NOT NULL,
  `departure_time` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bustracking_user`
--

DROP TABLE IF EXISTS `bustracking_user`;
CREATE TABLE IF NOT EXISTS `bustracking_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabla de usuarios de la aplicación.';

--
-- Volcado de datos para la tabla `bustracking_user`
--

INSERT INTO `bustracking_user` (`id_user`, `username`, `password`, `firstname`, `lastname`, `email`) VALUES
(1, 'administrator', 'f05f8d0353eaeae3e26b347287f6f7f8d3144c96', 'Administrador', 'del Sitio', 'administrator@bustracking.epizy.com');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
